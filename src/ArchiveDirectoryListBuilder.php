<?php

namespace Drupal\mp3archive;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Archive directory entities.
 *
 * @ingroup mp3archive
 */
class ArchiveDirectoryListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Archive directory ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\mp3archive\Entity\ArchiveDirectory */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.archive_directory.edit_form',
      ['archive_directory' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
