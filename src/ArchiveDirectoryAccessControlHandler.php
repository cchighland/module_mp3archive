<?php

namespace Drupal\mp3archive;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Archive directory entity.
 *
 * @see \Drupal\mp3archive\Entity\ArchiveDirectory.
 */
class ArchiveDirectoryAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\mp3archive\Entity\ArchiveDirectoryInterface $entity */
    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view archive directory entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit archive directory entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete archive directory entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add archive directory entities');
  }

}
