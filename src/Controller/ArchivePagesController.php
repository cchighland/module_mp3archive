<?php

namespace Drupal\mp3archive\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\mp3archive\ArchiveBuilderService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the Archive Pages Controller.
 *
 * @package Drupal\mp3archive\Controller
 */
class ArchivePagesController extends ControllerBase {

  /**
   * Drupal\mp3archive\ArchiveBuilderService definition.
   *
   * @var \Drupal\mp3archive\ArchiveBuilderService
   */
  protected $builder;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('mp3archive.builder')
    );
  }

  /**
   * Class constructor.
   *
   * @param \Drupal\mp3archive\ArchiveBuilderService $builder
   *   The archive builder service.
   */
  public function __construct(ArchiveBuilderService $builder) {
    $this->builder = $builder;
  }

  /**
   * Displaydirectory.
   *
   * @return string
   *   Return Hello string.
   */
  public function displayDirectory($id) {
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Implement method: displayDirectory with parameter(s): $id'),
    ];
  }

  /**
   * Displayfile.
   *
   * @return string
   *   Return Hello string.
   */
  public function displayFile($id) {
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Implement method: displayFile with parameter(s): $id'),
    ];
  }

}
