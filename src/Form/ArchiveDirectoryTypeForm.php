<?php

namespace Drupal\mp3archive\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ArchiveDirectoryTypeForm.
 */
class ArchiveDirectoryTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $archive_directory_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $archive_directory_type->label(),
      '#description' => $this->t("Label for the Archive directory type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $archive_directory_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\mp3archive\Entity\ArchiveDirectoryType::load',
      ],
      '#disabled' => !$archive_directory_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $archive_directory_type = $this->entity;
    $status = $archive_directory_type->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Archive directory type.', [
          '%label' => $archive_directory_type->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Archive directory type.', [
          '%label' => $archive_directory_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($archive_directory_type->toUrl('collection'));
  }

}
