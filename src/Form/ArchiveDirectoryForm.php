<?php

namespace Drupal\mp3archive\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Archive directory edit forms.
 *
 * @ingroup mp3archive
 */
class ArchiveDirectoryForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\mp3archive\Entity\ArchiveDirectory */
    $form = parent::buildForm($form, $form_state);

    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Archive directory.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Archive directory.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.archive_directory.canonical', ['archive_directory' => $entity->id()]);
  }

}
