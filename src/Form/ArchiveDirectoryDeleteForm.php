<?php

namespace Drupal\mp3archive\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Archive directory entities.
 *
 * @ingroup mp3archive
 */
class ArchiveDirectoryDeleteForm extends ContentEntityDeleteForm {


}
