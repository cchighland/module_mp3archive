<?php

namespace Drupal\mp3archive\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Archive directory entity.
 *
 * @ingroup mp3archive
 *
 * @ContentEntityType(
 *   id = "archive_directory",
 *   label = @Translation("Archive directory"),
 *   bundle_label = @Translation("Archive directory type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\mp3archive\ArchiveDirectoryListBuilder",
 *     "views_data" = "Drupal\mp3archive\Entity\ArchiveDirectoryViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\mp3archive\Form\ArchiveDirectoryForm",
 *       "add" = "Drupal\mp3archive\Form\ArchiveDirectoryForm",
 *       "edit" = "Drupal\mp3archive\Form\ArchiveDirectoryForm",
 *       "delete" = "Drupal\mp3archive\Form\ArchiveDirectoryDeleteForm",
 *     },
 *     "access" = "Drupal\mp3archive\ArchiveDirectoryAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\mp3archive\ArchiveDirectoryHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "archive_directory",
 *   admin_permission = "administer archive directory entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "label" = "name",
 *   },
 *   links = {
 *     "canonical" = "/archive/directory/{archive_directory}",
 *     "add-page" = "/archive/directory/add",
 *     "add-form" = "/archive/directory/add/{archive_directory_type}",
 *     "edit-form" = "/archive/directory/{archive_directory}/edit",
 *     "delete-form" = "/archive/directory/{archive_directory}/delete",
 *     "collection" = "/admin/config/mp3archive/directories",
 *   },
 *   bundle_entity_type = "archive_directory_type",
 *   field_ui_base_route = "entity.archive_directory_type.edit_form"
 * )
 */
class ArchiveDirectory extends ContentEntityBase implements ArchiveDirectoryInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Directory ID'))
      ->setDescription(t('The ID of the directory.'))
      ->setReadOnly(TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Archive directory entity.'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['path'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The path of the Archive directory entity.'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
