<?php

namespace Drupal\mp3archive\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface for defining Archive directory entities.
 *
 * @ingroup mp3archive
 */
interface ArchiveDirectoryInterface extends ContentEntityInterface, EntityChangedInterface {

  /**
   * Gets the Archive directory name.
   *
   * @return string
   *   Name of the Archive directory.
   */
  public function getName();

  /**
   * Sets the Archive directory name.
   *
   * @param string $name
   *   The Archive directory name.
   *
   * @return \Drupal\mp3archive\Entity\ArchiveDirectoryInterface
   *   The called Archive directory entity.
   */
  public function setName($name);

  /**
   * Gets the Archive directory creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Archive directory.
   */
  public function getCreatedTime();

  /**
   * Sets the Archive directory creation timestamp.
   *
   * @param int $timestamp
   *   The Archive directory creation timestamp.
   *
   * @return \Drupal\mp3archive\Entity\ArchiveDirectoryInterface
   *   The called Archive directory entity.
   */
  public function setCreatedTime($timestamp);

}
