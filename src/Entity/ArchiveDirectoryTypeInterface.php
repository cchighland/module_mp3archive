<?php

namespace Drupal\mp3archive\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Archive directory type entities.
 */
interface ArchiveDirectoryTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
