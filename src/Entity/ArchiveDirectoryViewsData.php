<?php

namespace Drupal\mp3archive\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Archive directory entities.
 */
class ArchiveDirectoryViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
