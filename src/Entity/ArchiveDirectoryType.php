<?php

namespace Drupal\mp3archive\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Archive directory type entity.
 *
 * @ConfigEntityType(
 *   id = "archive_directory_type",
 *   label = @Translation("Archive directory type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\mp3archive\ArchiveDirectoryTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\mp3archive\Form\ArchiveDirectoryTypeForm",
 *       "edit" = "Drupal\mp3archive\Form\ArchiveDirectoryTypeForm",
 *       "delete" = "Drupal\mp3archive\Form\ArchiveDirectoryTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\mp3archive\ArchiveDirectoryTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "archive_directory_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "archive_directory",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/media/mp3archive/directory_type/{archive_directory_type}",
 *     "add-form" = "/admin/config/media/mp3archive/directory_type/add",
 *     "edit-form" = "/admin/config/media/mp3archive/directory_type/{archive_directory_type}/edit",
 *     "delete-form" = "/admin/config/media/mp3archive/directory_type/{archive_directory_type}/delete",
 *     "collection" = "/admin/config/media/mp3archive/directory_type"
 *   }
 * )
 */
class ArchiveDirectoryType extends ConfigEntityBundleBase implements ArchiveDirectoryTypeInterface {

  /**
   * The Archive directory type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Archive directory type label.
   *
   * @var string
   */
  protected $label;

}
