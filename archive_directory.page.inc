<?php

/**
 * @file
 * Contains archive_directory.page.inc.
 *
 * Page callback for Archive directory entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Archive directory templates.
 *
 * Default template: archive_directory.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_archive_directory(array &$variables) {
  // Fetch ArchiveDirectory Entity Object.
  $archive_directory = $variables['elements']['#archive_directory'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
